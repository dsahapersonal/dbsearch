<?php

require "vendor/autoload.php";
use debojyoti\DbSearch;

$db_credentials = ["database"=>"test_db",
                    "username"=>"root",
                    "password"=>"",
                    "host"=>"localhost"];

$search = new DbSearch($db_credentials);


if(isset($_GET["search"],$_GET['submit'])) {
    $search_result = $search->fetch("test_table",$_GET["search"]);
}
// print_r($search->fetch("test_table","aa"));

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form method="GET" action="index.php">
        <input type="text" placeholder="text" name="search">
        <input type="submit" name="submit" value="search">
    </form>
    <?php
        if(isset($search_result) && count($search_result)) {
            foreach($search_result as $key=>$value) {
                foreach($value as $a=>$b) {
                    echo $a." : ".$b." , ";
                }
                echo "<br>";
            }
        }
    ?>
</body>
</html>
