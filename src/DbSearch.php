<?php

namespace Debojyoti;
/*
Simple class to handle customized searches on a selected table of database
*/

use Debojyoti\PdoConnect\Handler;

class DbSearch {
    private $db;

    public function __construct(array $db_credentials = null) {
        if (isset($db_credentials)) {
            $this->db = new Handler($db_credentials);
        } else {
            $this->db = new Handler();
        }
    }

    public function fetch($table, $search_string, array $ignore_columns = null) {
        $rows = $this->db->getRowsData($table);
        if ($ignore_columns) {
            $rows = array_diff($rows, $ignore_columns);
        }
        $search_array = $this->genSearchArray($rows,$search_string);
        // print_r($search_array);
        return $this->db->getDataOr($table,$search_array, $rows);
    }

    private function genSearchArray($rows, $search_string) {
        $search_array = [];
        if($search_string!="") {
            foreach($rows as $key=>$value) {
                $search_array[$value] = '~'.$search_string;
            }
        }
        
        return $search_array;
    }
}